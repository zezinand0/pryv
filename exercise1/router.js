/**
 * Title: Router
 * Description: Handles the routing system for the /data resource.
 * Date: 17.03.2020
 * Author: Natanael Xufre Garcez
 */
let pryvUserController = require('./pryvUserController.js');
var express = require('express');
var router = express.Router();

// Handles a POST request on the root (/) of the /data resource
router.post('/', function(request, response) {

    let credentials = '';

    // Gets the body of the request
    request.on('data', chunk => {
        credentials += chunk.toString(); // convert Buffer to string
    });

    request.on('end', () => {
        credentials = JSON.parse(credentials);

        // First fetch both accounts
        new Promise(function(resolve, reject) {
            pryvUserController.methods.fetchAccounts(credentials, resolve, reject);
        }).then(function(response){
            console.log(response);
            createEvent(response.bothAccounts,response.streamId)
        }).catch(err => {
            writeResponse(err)
        });

        // After fetching the accounts, create the Event
        function createEvent(bothAccounts, streamId){
            new Promise(function(resolve, reject) {
                pryvUserController.methods.createEvent(credentials, bothAccounts, streamId, resolve, reject);
            }).then(function(resp){
                writeResponse(resp)
            }).catch(err => {
                writeResponse(err)
            });
        }

        // Writes the response and ends the request
        function writeResponse(resp){
            console.log(resp);
            response.writeHead(resp.status,{'Content-Type': 'application/json'});
            response.write(JSON.stringify(resp.body,undefined,4));
            response.end()
        }
    });
});
module.exports = router;