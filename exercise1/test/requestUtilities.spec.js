/**
 * Title: pryvUserController.spec.js
 * Description: Tests the workability of the exercise
 * Date: 19.03.2020
 * Author: Natanael Xufre Garcez
 */
let requestUtilities = require('../requestUtilities.js');

var expect  = require('chai').expect;
var assert = require('chai').assert;

describe('requestUtilities test', function(){

    let result = {
        'status': 403,
        'statusText': "Forbidden",
        'url':  "Some URL",
    };
    it('It should throw an error when status code !- 200-299', function(done) {
      try{
          requestUtilities.module.checkRequestStatus(result);
      }catch (error) {
          expect(error.status).to.equal(403);
          expect(error.message).to.equal("Forbidden");
          expect(error.body.url).to.equal("Some URL");
          done()
      }
    });

});


