'use strict';
/**
 * Title: pryvUserController.spec.js
 * Description: Tests the workability of the exercise
 * Date: 19.03.2020
 * Author: Natanael Xufre Garcez
 */
var expect  = require('chai').expect;
var assert = require('chai').assert;
var request = require('request');
let pryvUserController = require('../pryvUserController.js');
let testAccounts = require('./accounts');
let credentials = require('./credentials');
let badCredentials = require('./badCredentials');

describe('/data resource', function(){

    it('Makes a request to /data and creates an event', function(done) {
        this.timeout(20000);
        request.post(
            {
                uri:'http://localhost:8081/data',
                json:true,
                body:credentials
            },
            function(error, response, body) {
                let resource = JSON.parse(response.request.body);
                assert.typeOf(resource.source.username, 'string');
                assert.typeOf(resource.source.token, 'string');
                assert.typeOf(resource.backup.username, 'string');
                assert.typeOf(resource.backup.token, 'string');

                expect(response.statusCode).to.equal(201);
                expect(response.statusMessage).to.equal("Created");
                done();
            }
        );
    });

    it('The access to / should be forbidden ', function(done) {
        this.timeout(20000);
        request.get({uri:'http://localhost:8081/'}, function(error, response, body) {
            expect(response.statusCode).to.equal(404);
            done();
        });
    });

    it('Should fail with bad credentials', function(done){
        this.timeout(20000);
        request.post(
            {
                uri:'http://localhost:8081/data',
                json:true,
                body:badCredentials
            },
            function(error, response, body) {
                let resource = JSON.parse(response.request.body);
                assert.typeOf(resource.source.username, 'string');
                assert.typeOf(resource.source.token, 'string');
                assert.typeOf(resource.backup.username, 'string');
                assert.typeOf(resource.backup.token, 'string');

                expect(response.statusCode).to.equal(403);
                expect(response.statusMessage).to.equal("Forbidden");
                done();
            }
        );
    })

    it('Fetch accounts - Should fetch 2 accounts with good credentials', function(done) {
        new Promise(function(resolve, reject) {
            pryvUserController.methods.fetchAccounts(credentials, resolve, reject);
        }).then(function(response){
           expect(response.bothAccounts.length).to.equal(2);
           expect(response.streamId).to.equal("a");
           done()
        })
    });

    it('Fetch accounts - Should fail fetching the 2 accounts because one has bad credentials', function(done) {
        new Promise(function(resolve, reject) {
            pryvUserController.methods.fetchAccounts(badCredentials, resolve, reject);
        }).then(function(response){
            done()
        }).catch(function(response){
            expect(response.status).to.equal(403);
            expect(response.message).to.equal("Forbidden");
            expect(response.body.url).to.equal("https://sw-interview-backup.pryv.me/streams?auth=A%20very%20bad%20token");
            done()
        })
    });

    it('Create Event - Should show status 201 - Created', function(done) {
        let streamId = "a";
        new Promise(function(resolve, reject) {
            pryvUserController.methods.createEvent(credentials, testAccounts, streamId, resolve, reject);
        }).then(function(response){
            expect(response.status).to.equal(201);
            expect(response.message).to.equal("Created");
            console.log(JSON.stringify(response.body,undefined,4));
            done()
        })
    });

    it('Create Event - Fails to create an event with bad credentials', function(done) {
        let streamId = "a";
        new Promise(function(resolve, reject) {
            pryvUserController.methods.createEvent(badCredentials, testAccounts, streamId, resolve, reject);
        }).catch(function(response){
            expect(response.status).to.equal(403);
            expect(response.message).to.equal("Forbidden");
            done()
        })
    });
});


