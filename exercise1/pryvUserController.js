/**
 * A library with the methods a pryv user needs to fetch streams and create Events.
 * @author Natanael Xufre Garcez
 * @module pryvUserController
 * @see module:pryvUserController
 */
let fetch = require("node-fetch");
let requestUtilities = require('./requestUtilities.js');
let methods = {
    /**
     * Will fetch Stream and Backup accounts, concatenates them and creates an Event with it.
     * @function fetchAccounts
     * @param {JsonObject} credentials - The credentials
     * @param {Promise} completer - The callback function
     * @param {Promise} rejectCompleter - The callback error function
     * @returns callBackResponse - An object with the fetched Stream and Backup accounts and the streamId.
     */
    fetchAccounts: function(credentials, completer, rejectCompleter){
        // The return value that goes with the completer callback
        let callbackResponse = {};
        let accountFetchRequestPromises = [];

        // The source and backup Accounts
        let resources = [];
        resources[0] = credentials.source;
        resources[1] = credentials.backup;

        // Fetch both Source and Backup accounts
        for(let i = 0; i <2; i++){
            let accountFetchRequest =  new Promise(function(resolve,reject){
                fetch('https://'+resources[i].username+'.pryv.me/streams?auth='+resources[i].token)
                    .then(requestUtilities.module.checkRequestStatus)
                    .then(res => res.json())
                    .then(json => {
                        console.log(json);
                        resolve(json)
                    }).catch(err => {
                        console.log(err);
                        reject(err);
                });
            });
            accountFetchRequestPromises.push(accountFetchRequest)
        }

        Promise.all(accountFetchRequestPromises).then(function(values) {
            /* The variable "values" contains the objects returned on the promises. We'll concatenate them after
               pushing them all into the accounts array.*/
            let accounts = [];
            values.forEach(function (promise) {
                promise.streams.forEach(function (stream) {
                    accounts.push(stream)
                })
            });

            // The streamId
            let streamId = values[0].streams[0].id;
            // Concatenate both accounts
            let sourceAndBackup = Object.assign(accounts);
            callbackResponse.bothAccounts = sourceAndBackup;
            callbackResponse.streamId = streamId;
            completer(callbackResponse)
        }).catch(function(error){
                /* One of the promises failed.  Return to the callback on router.js with an error object
                 in order to end the request */
                rejectCompleter(error)
        });
    },

    /**
     * Creates an Event on Pryv API
     * @function createEvent
     * @param {JsonObject} credentials - The credentials
     * @param {JsonObject} accounts - Both Stream and Backup Accounts
     * @param {number} streamId - The streamId
     * @param {Promise} completer - The callback function
     * @param {Promise} rejectCompleter - The callback error function
     * @returns callBackResponse - The response from the API with the created Event.
     */
    createEvent: function(credentials, accounts, streamId, completer, rejectCompleter){

        // The return value that goes with the completer callback
        let callbackResponse = {};

        // The new event will be a concatenation of the Source and Backup accounts that will be fetched later.
        let newEvent = {};
        newEvent['streamId'] = streamId;
        newEvent['type'] = "exercice-1/streams";
        newEvent['content'] = accounts;

        // Create the Event by fetching the API
        fetch('https://'+credentials.backup.username+'.pryv.me/events', {
            method: 'post',
            body: JSON.stringify(newEvent),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': credentials.backup.token
            }
        }).then(requestUtilities.module.checkRequestStatus)
            .then(res => {
                // Complete the httpResponse
                callbackResponse.status = res.status;
                callbackResponse.message = res.statusText;
                return res.json()
            })
            .then(json => {
                // Complete the httpResponse with the Event created on the API
                callbackResponse.body = json.event;

                //Returns to the callback on router.js with an error object in order to end the request
                completer(callbackResponse);
            }).catch(err => {
            console.log(err);
            // Returns to the callback on router.js in order to end the request
            rejectCompleter(err)
        });
    }
};
exports.methods = methods;