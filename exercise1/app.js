/**
 * Title: NodeJs Server
 * Description: Instantiates the NodeJS Server that listens on the port 8081, with the help of the http module.
 * Date: 17.03.2020
 * Author: Natanael Xufre Garcez
 */

var express = require('express');
var app = express();
var router = require('./router');

// Launch listening server on port 8081
app.listen(8081, function () {
    console.log('App listening on port 8081')
});

// Handles every request matching /data and it sub resources
app.use('/data', router);

module.exports = app;