/**
 * A library that allows to check the status of the request. It handles only
 * the 200-299 status code. Throws an error if not in (200-299).
 * @author Natanael Xufre Garcez
 * @module requestUtilities
 * @see module:requestUtilities
 */
let requestUtilities = {
    /**
     * Checks the status of the request and throws an error if not in the 200-299 range.
     * @param result
     * @returns an error
     */
    /**
     * Checks the status of the request and throws an error if not 200
     * @function checkRequestStatus
     * @param {JsonObject} result - The result of the request
     * @returns result - Returns the result.
     */
    checkRequestStatus: function (result) {
        // Manages only 200 range codes. Redirection (3**) are not supported.
        if (result.status < 200 || result.status > 299) {
            let error = {
                'status': result.status,
                'message': result.statusText,
                'body': {'url':result.url},
            };
            throw error;
        } else{
            return result
        }
    }
};
exports.module = requestUtilities;
